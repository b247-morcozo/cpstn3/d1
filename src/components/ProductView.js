import { useParams, useNavigate, Link } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [userOrders, setuserOrders] = useState(0);

const checkout = (productId) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			productId: productId,
			userId: user.id // Add user ID here
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(data === true){
			Swal.fire({
				title: "success",
				icon: "success",
				text: "You have order the product successfully."
			})
			navigate("/");
		} else {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again"
			})
		}
	})
}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/details`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setuserOrders(data.userOrders.length);
		})
	}, [productId])

	return (
		<Container>
				<h1 className="text-center">CART</h1>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        {
					        	(user.id !== null) ?
					        		<>
					        		<Button variant="primary" onClick={() => checkout(productId)} >Checkout</Button>
					        		</>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login">Log in to Add to Cart</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
};