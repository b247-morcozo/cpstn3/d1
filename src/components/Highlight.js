import { Card, Row, Col } from 'react-bootstrap';

export default function Highights() {
  return (
    <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Spring Sale 25% OFF</h2>
                    </Card.Title>
                    <Card.Text>
                        Image here ... 
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>March Madness</h2>
                    </Card.Title>
                    <Card.Text>
                        Free to play SHOWCASE
                        
                                X

                            March into the hottest 
                            free to play games
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>DISCOVER OUT OF THIS WORLD SUBS</h2>
                    </Card.Title>
                    <Card.Text>
                        Enjoy hundreds of classic games, online multiplayer, and more unmissable benefits.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  );
}