import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useEffect, useState, useContext } from 'react';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavbar() {

  const { user } = useContext(UserContext);
  console.log(user);

  return (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">SUB-CENTRAL</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/">HOME</Nav.Link>
            <Nav.Link as={NavLink} to="/products">SHOP</Nav.Link>
            
            { (user.id === null) ?
              <>
              <Nav.Link as={NavLink} to="/login">LOGIN</Nav.Link>
              <Nav.Link as={NavLink} to="/register">REGISTER</Nav.Link>
              </>
              :
              (user.isAdmin) ?
              <>
              <Nav.Link as={NavLink} to="/products/add">ADD ITEM</Nav.Link>
              <Nav.Link as={NavLink} to="/logout">LOGOUT</Nav.Link>
              </>
              :
              <>
              <Nav.Link as={NavLink} to="/logout">LOGOUT</Nav.Link>  
              </>            
            }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}