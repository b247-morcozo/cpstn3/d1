import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import ProductView from './components/ProductView'
import Product from './pages/Product'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import AddProduct from './pages/AddProduct'
import UpdateProduct from './pages/UpdateProduct'
import ArchiveProduct from './pages/ArchiveProduct'
import ActivateProduct from './pages/ActivateProduct'
import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem(`token`)}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser ({
          id:null,
          isAdmin:null
        })
      }
    })
  }, []);

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
        <Container>
          <Routes>
           <Route path="/" element={<Home />} />
            <Route path="/products" element={<Product/>} />
            <Route path="/products/:productId" element={<ProductView/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/products/add" element={<AddProduct/>} />
            <Route path="/products/:productId/update" element={<UpdateProduct/>} />
            <Route path="/products/:productId/archive" element={<ArchiveProduct/>} />
            <Route path="/products/:productId/activate" element={<ActivateProduct/>} />
            <Route path="/" element={<Logout/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
