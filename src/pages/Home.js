import Banner from '../components/Banner';
import Highlight from '../components/Highlight';

export default function Home(){

	const data = {
		title: "Subscription Central",
		content: "Don’t miss deals on thousands of games and add-ons.",
		destination: "/products",
		label: "Browse"
	}
	
	return (
		<>
			<Banner data={data} />
			<Highlight />
		</>
	)
}