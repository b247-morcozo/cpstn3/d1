import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';

import { useState, useEffect } from 'react';

export default function Products() {
	
const [ products, setProducts ] = useState([]);
	const [ isLoading, setIsLoading] = useState(true);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))

			setIsLoading(false);
		})
	}, [])

	return(
		(isLoading) ?
			<Loading />
		:
		<>
			{products}
		</>
	)

}