import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function AddProduct () {

	const[name,setName]= useState('');
	const[description,setDescription]= useState('');
	const[price,setPrice]= useState('');

	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function addProduct (e){
		fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				Swal.fire({
					title: "Product Added",
					icon: "success",
					text: "Successfully"
				})

				setName("");
				setDescription("");
				setPrice("");
				navigate("/add")
			} else {

				Swal.fire({

					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	return(
		<Form onSubmit={(e)=> addProduct(e)}>


		        <Form.Group controlId="productName" className="my-2">
			        <Form.Label>Name</Form.Label>
				        <Form.Control 
					        type="text" 
					        placeholder="Enter Product Name"
					        value = {name}
					        onChange = {e => setName(e.target.value)} 
					        required
				        /> 
		        </Form.Group>

	        <Form.Group controlId="productDescription" className="my-2">
	        	<Form.Label>Description</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder="Enter Product Description"
				        value = {description}
				        onChange = {e => setDescription(e.target.value)} 
				        required
				    /> 
	        	</Form.Group>

	        <Form.Group controlId="productPrice" className="my-2">
		        <Form.Label>Price</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder=" "
				        value = {price}
				        onChange = {e => setPrice(e.target.value)} 
				        required
				        />
	        </Form.Group>

	        <Button variant="primary" type="submit" id="submitBtn" >
	        Submit
	        </Button>

	</Form>
	)
}
