import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ArchiveProduct () {
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  function archiveProduct (e){
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/:id/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        isActive: isActive
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data) {
        Swal.fire({
          title: "Product Archived",
          icon: "success",
          text: "Successfully"
        })
        setIsActive(false);
        navigate("/")
      } else {
        Swal.fire({
          title: "Something went wrong!",
          icon: "error",
          text: "Please try again!"
        })
      }
    })
  }

  return (
    <Form onSubmit={archiveProduct}>
      <Button variant="primary" type="submit" id="submitBtn">
        Archive
      </Button>
    </Form>
  )
}
